$("#contactForm, #contactForm-2").validator().on("submit", function (event) {
    if (event.isDefaultPrevented()) {
        // handle the invalid form...
        formError();
        submitMSG(false, "Did you fill in the form properly?", this);
    } else {
        // everything looks good!
        event.preventDefault();
        submitForm(this);
    }
});


function submitForm($form){
    // Initiate Variables With Form Content
    var name = $("#name").val();
    var email = $("#email").val();
    var msg_subject = $("#msg_subject").val();
    var message = $("#message").val();
    var
        data = {}

    $('input,select', $form).each(function() {
        var
            val = $(this).val(),
            name = $(this)[0].getAttribute('name')

        data[name] = val;
    })

    $.ajax({
        type: "POST",
        url: "php/form-process.php",
        data: data,
        success : function(text){
            if (text == "success"){
                formSuccess($form);
            } else {
                formError();
                submitMSG(false,text, $form);
            }
        }
    });
}

function formSuccess($form){
    if ($($form).data('form-id')) {
        $("#contactForm-2")[0].reset();
        submitMSG(true, "Infomration Submited!", $form)
    } else {
        $("#contactForm")[0].reset();
        submitMSG(true, "Message Submitted!", $form)
    }

}

function formError(){
    $("#contactForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();
    });
}

function submitMSG(valid, msg, $form){
    if(valid){
        var msgClasses = "h3 text-center tada animated text-success";
    } else {
        var msgClasses = "h3 text-center text-danger";
    }
    $("#msgSubmit", $form).removeClass().addClass(msgClasses).text(msg);
}
