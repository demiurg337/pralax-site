(function($) {
  
  "use strict";

  // Sticky Nav
  /*
    $(window).on('scroll', function() {
        if ($(window).scrollTop() > 10) {
            $('.scrolling-navbar').addClass('top-nav-collapse');
        } else {
            $('.scrolling-navbar').removeClass('top-nav-collapse');
        }
    });
    */
    /* 
   One Page Navigation & wow js
   ========================================================================== */
    //Initiat WOW JS
    new WOW().init();

    // one page navigation 
    $('.main-navigation').onePageNav({
            currentClass: 'active'
    }); 

    $(window).on('load', function() {
       
        $('body').scrollspy({
            target: '.navbar-collapse',
            offset: 195
        });

        $(window).on('scroll', function() {
            if ($(window).scrollTop() > 10) {
                $('.fixed-top').addClass('menu-bg');
            } else {
                $('.fixed-top').removeClass('menu-bg');
            }
        });

    });

    // Slick Nav 
    $('.mobile-menu').slicknav({
      prependTo: '.navbar-header',
      parentTag: 'span',
      allowParentLinks: true,
      duplicate: false,
      label: '',
    });


/* 
   CounterUp
   ========================================================================== */
    $('.counter').counterUp({
      time: 1000
    });

/* 
   MixitUp
   ========================================================================== */
  $('#portfolio').mixItUp();
/* 
   Touch Owl Carousel
   ========================================================================== */
    var owl = $("#slider-1");




    owl.owlCarousel({
      //navigation: false,
      pagination: true,
      smartSpeed: 1000,
      fluidSpeed: 100,
      /*
      slideSpeed: 5000,
      */
      stopOnHover: true,
      autoPlay: true,
      //items: 2,
      items: 1,
      itemsDesktop : [1199,1],
      itemsDesktopSmall: [1024, 1],
      itemsTablet: [600, 1],
      itemsMobile: [479, 1],
      nav: true,
    });

var
    animationClass,
    prevoius = 0
owl.on('translated.owl.carousel', function(event) {
    var
        currentPage = 0;
        
    if ( 'page' in event) {
        currentPage = event.page.index 
    }

    if (animationClass) {
        $(".animated", owl).removeClass('animated ' + animationClass);
    }

    if (currentPage > prevoius) {
        animationClass = 'fadeInRightBig'
    } else {
        animationClass = 'fadeInLeftBig'
    } 

    prevoius = currentPage
    /*
    console.log(event.item)
    console.log(event.page)
    console.log(event)
    console.log($(".active .testimonial-text h3", this));
    */
    $(".active .testimonial-text h3", this).addClass('animated ' + animationClass);
    $(".active .testimonial-text p", this).addClass('animated ' + animationClass);
})
owl.trigger('translated.owl.carousel')
//
//next.owl.carousel

    $('.touch-slider').find('.owl-prev').html('<i class="fa fa-chevron-left"></i>');
    $('.touch-slider').find('.owl-next').html('<i class="fa fa-chevron-right"></i>');
    
    /*
    var owl2 = $("#content-slider");
    owl2.owlCarousel({
      //navigation: false,
      pagination: true,
      slideSpeed: 1000,
      stopOnHover: true,
      autoPlay: true,
      //items: 2,
      items: 1,
      itemsDesktop : [1199,1],
      itemsDesktopSmall: [1024, 1],
      itemsTablet: [600, 1],
      itemsMobile: [479, 1],

      dotsContainer: '#content-slider-dots'
    });

    $('.touch-slider').find('.owl-prev').html('<i class="fa fa-chevron-left"></i>');
    $('.touch-slider').find('.owl-next').html('<i class="fa fa-chevron-right"></i>');


    var i = 0;
    $('.owl-dot', '#content-slider-dots').each(function() {
        var
            html = ''

        if (i=== 0) {
            html = '<i class="lnr lnr-cog"></i>'
        } else if (i === 1) {
            html = '<i class="lnr lnr-rocket"></i>'
        } else {
            html = '<i class="lnr lnr-layers"></i>'
        }
        
        
        $(this).html(html)
        i++;
    })
    */

/* 
   Sticky Nav
   ========================================================================== */
    $(window).on('scroll', function() {
        if ($(window).scrollTop() > 10) {
            $('.header-top-area').addClass('menu-bg');
        } else {
            $('.header-top-area').removeClass('menu-bg');
        }
    });

/* 
   VIDEO POP-UP
   ========================================================================== */
    $('.video-popup').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false,
    });


  /* 
   SMOOTH SCROLL
   ========================================================================== */
    var scrollAnimationTime = 1200,
        scrollAnimation = 'easeInOutExpo';

    $('a.scrollto').on('bind', 'click.smoothscroll', function (event) {
        event.preventDefault();
        var target = this.hash;
        
        $('html, body').stop().animate({
            'scrollTop': $(target).offset().top
        }, scrollAnimationTime, scrollAnimation, function () {
            window.location.hash = target;
        });
    });

/* 
   Back Top Link
   ========================================================================== */
    var offset = 200;
    var duration = 500;
    $(window).scroll(function() {
      if ($(this).scrollTop() > offset) {
        $('.back-to-top').fadeIn(400);
      } else {
        $('.back-to-top').fadeOut(400);
      }
    });

    $('.back-to-top').on('click',function(event) {
      event.preventDefault();
      $('html, body').animate({
        scrollTop: 0
      }, 600);
      return false;
    })

/* Nivo Lightbox
  ========================================================*/   
   $('.lightbox').nivoLightbox({
    effect: 'fadeScale',
    keyboardNav: true,
  });


/* stellar js
  ========================================================*/
  $.stellar({
    horizontalScrolling: true,
    verticalOffset: 40,
    responsive: true
  });

/* 
   Page Loader
   ========================================================================== */
  $('#loader').fadeOut();

}(jQuery));

