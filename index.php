<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Megatail</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/line-icons.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/nivo-lightbox.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/slicknav.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/main.css">    
    <link rel="stylesheet" href="css/responsive.css">
    <style>



    </style>

  </head>
  <body>
    <?php 
        if (strpos($_SERVER['PATH_INFO'], 'login') !== false):?>

            <?php if (strpos($_SERVER['PATH_INFO'], 'es') === false):?>
            <style>
                .caption_en{
                  font-size: 18px;   
                  background-color:#29597f;
                }
                main {
                    box-sizing: border-box;
                    height: 100vh;
                    display: flex;
                    flex-direction: row;
                    justify-content: center;
                    align-items: center;  
                }
                h2 {
                    font-family: "Proxima Nova", sans-serif;
                    font-size: 30px;
                    line-height: 4rem;
                    font-weight: 700;
                    font-style: normal;
                    text-align: center;
                    text-decoration: none;
                        color: #fff;
                  text-shadow:5px 5px 5px #000000;
                  
                }
            </style>

            <div class="caption_en">
                <main>
                    <h2>You need our license accepted to login. 
                      <br>Cameras and sensors must be installed and configured</h2>
                </main>
            </div>
            <?php else: ?>
            <style>
            .caption_spanish{
              font-size: 18px;   
              background-color:#29597f;
            }
            main {
                box-sizing: border-box;
                height: 100vh;
                display: flex;
                flex-direction: row;
                justify-content: center;
                align-items: center;  
            }
            h2 {
                font-family: "Proxima Nova", sans-serif;
                font-size: 30px;
                line-height: 4rem;
                font-weight: 700;
                font-style: normal;
                text-align: center;
                text-decoration: none;
                    color: #fff;
              text-shadow:5px 5px 5px #000000;
              
            }
            </style>
            <div class="caption_spanish">
                <main>
                <h2>Tienes que tener nuestra licencia aceptada previamente. <br>Asegurate que las cámaras y sensores estén instalados correctamente para poder acceder</h2>
              
             
              </main>
              </div>
            <?php endif; ?>
        <?php else: ?>
    <!-- Header Section Start -->
    <header id="hero-area" data-stellar-background-ratio="0.5">    
      <!-- Navbar Start -->
      <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <a href="index.html" class="navbar-brand"><img class="img-fulid" src="img/logo.png" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
              <i class="lnr lnr-menu"></i>
            </button>
          </div>
          <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="navbar-nav mr-auto w-100 justify-content-end">
              <li class="nav-item">
                <a class="nav-link page-scroll" href="#hero-area">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link page-scroll" href="#services">Services</a>
              </li>
              <li class="nav-item">
                <a class="nav-link page-scroll" href="#features">Features</a>
              </li>
              <li class="nav-item">
                <a class="nav-link page-scroll" href="#contact">Contact</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/login">Dashboard</a>
              </li>
              <li class="nav-item">
                <a class="nav-link page-scroll nav-link-btn" href="#user-form">Schedule a Demo</a>
              </li>

              <li class="nav-item">
              <ul class="header__flag-icons">
                <li class="header__flag-icon  header__flag-icon_argentina header__flag-icon_selected">
                    <a href="/" title="Argentina">
                        Argentina
                    </a>
                </li>
                <li class=" header__flag-icon  header__flag-icon_english ">
                    <a href="/" title="English">
                        English
                    </a>
                </li>
              </li>
            </ul>
          </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class="mobile-menu">
           <li>
              <a class="page-scroll" href="#hero-area">Home</a>
            </li>
            <li>
              <a class="page-scroll" href="#services">Services</a>
            </li>
            <li>
              <a class="page-scroll" href="#features">Features</a>
            </li>
            <li>
              <a class="page-scroll" href="#portfolios">Works</a>
            </li>
            <li>
              <a class="page-scroll" href="#contact">Contact</a>
            </li>
            <li>
              <a class="page-scroll" href="/login">Dashboard</a>
            </li>
        </ul>
        <!-- Mobile Menu End -->

      </nav>
    <!-- testimonial Section Start -->
    <div id="testimonial" class="section" data-stellar-background-ratio="0.1">
      <div class="container-for-slider">
        <div class="row justify-content-md-center">
          <div class="col-md-12">
            <div id="slider-1" class="touch-slider owl-carousel owl-theme">
              <div class="testimonial-item">
                <img src="img/06.jpg" alt="Client Testimonoal" />
                <div class="testimonial-text">
                  <p>Improve the shopper experience and optimize your store profitability</p>
                  <h3>Jone Deam</h3>
                  <span>Fondor of Jalmori</span>
                </div>
              </div>
              <div class="testimonial-item">
                <img src="img/03.jpg" alt="Client Testimonoal" />
                <div class="testimonial-text">
                  <p>Understand your customer behaviour and increase your convertion rates</p>
                  <h3>Oidila Matik</h3>
                  <span>President Lexo Inc</span>
                </div>
              </div>
              <div class="testimonial-item">
                <img src="img/04.jpg" alt="Client Testimonoal" />
                <div class="testimonial-text">
                  <p>Online monitoring system. Drive shopper experience with personalized recommendations, offers and content</p>
                  <h3>Alex Dattilo</h3>
                  <span>CEO Optima Inc</span>
                </div>
              </div>
              <div class="testimonial-item">
                <img src="img/05.jpg" alt="Client Testimonoal" />
                <div class="testimonial-text">
                  <p>Predict consumption patterns changes and be able to align your operation and communication</p>
                  <h3>Paul Kowalsy</h3>
                  <span>CEO & Founder</span>
                </div>
              </div>
              <div class="testimonial-item">
                <img src="img/07.jpg" alt="Client Testimonoal" />
                <div class="testimonial-text">
                  <p>Take it Out</p>
                  <h3>Alex Dattilo</h3>
                  <span>CEO Optima Inc</span>
                </div>
              </div>
            </div>
          </div>
        </div>        
      </div>
    </div>
    <!-- testimonial Section Start -->

          
    </header>
    <!-- Header Section End --> 

    <!-- testimonial Section Start -->
    <!--
    <div  class="section" data-stellar-background-ratio="0.1">
      <div class="container-for-slider-with-info">
        <div class="row justify-content-md-center">
          <div class="col-md-12">
          
            <div id="content-slider-dots" class="owl-dots"></div>

            <div id="content-slider" class="touch-slider owl-carousel owl-theme">
              <div class="testimonial-item">
                <img src="img/04.jpg" alt="Client Testimonoal" />
                <div class="testimonial-text">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. send do <br> adipisicing ciusmod tempor incididunt ut labore et</p>
                  <h3>Alex Dattilo</h3>
                  <span>CEO Optima Inc</span>
                </div>
              </div>
              <div class="testimonial-item">
                <img src="img/05.jpg" alt="Client Testimonoal" />
                <div class="testimonial-text">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. send do <br> adipisicing ciusmod tempor incididunt ut labore et</p>
                  <h3>Paul Kowalsy</h3>
                  <span>CEO & Founder</span>
                </div>
              </div>
              <div class="testimonial-item">
                <img src="img/07.jpg" alt="Client Testimonoal" />
                <div class="testimonial-text">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. send do <br> adipisicing ciusmod tempor incididunt ut labore et</p>
                  <h3>Alex Dattilo</h3>
                  <span>CEO Optima Inc</span>
                </div>
              </div>
              </div>
            </div>
          </div>
        </div>        
      </div>
    </div>
    -->

    <!-- Services Section Start -->
    <section id="services" class="section">
      <div class="container">
        <div class="section-header">          
          <h2 class="section-title wow fadeIn" data-wow-duration="1000ms" data-wow-delay="0.3s">MEGATAIL SOLUTION</h2>
          <hr class="lines wow zoomIn" data-wow-delay="0.3s">
          <p class="section-subtitle wow fadeIn" data-wow-duration="1000ms" data-wow-delay="0.3s">The effective customer segmentation will allow you to use new communication channels, to send customized content before the shopper visit and to engage with them across the path-to-purchase.</p>
        </div>
        <div class="row">
          <div class="col-md-4 col-sm-6">
            <div class="item-boxes wow fadeInDown" data-wow-delay="0.2s">
              <div class="icon">
                <i class="lnr lnr-pencil"></i>
              </div>
              <h4>RETAIL ANALYTICS</h4>
              <p>Megatail software accumulates information from all your stores, organize, analyze and compare with market benchmark indicators – all in real time.</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="item-boxes wow fadeInDown" data-wow-delay="0.8s">
              <div class="icon">
                <i class="lnr lnr-code"></i>
              </div>
              <h4>VISUALIZATION & PREDICTIONS</h4>
              <p>User friendly platform to visualize data, variety of useful formats, accesibility from any device. Turn store data into insights by adding market intelligence. Discover predictions about new trends, customer segmentation and commercial offers.</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="item-boxes wow fadeInDown" data-wow-delay="1.2s">
              <div class="icon">
                <i class="lnr lnr-mustache"></i>
              </div>
              <h4>DYNAMIC RECOMMENDATIONS AND CUSTOMER ENGAGEMENT</h4>
              <p>Our machine learning engine provide real time recommendations for the retailers/mall owners and for shoppers too! You will be able to set dynamic prices and marketing campaigns. We include best staffing distribution suggestions to optimize the customer attendance at different time.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Services Section End -->

    <!-- Features Section Start -->
    <section id="features" class="section" data-stellar-background-ratio="0.2">
      <div class="container">
        <div class="section-header">          
          <h2 class="section-title">Some Features</h2>
          <hr class="lines">
          <p class="section-subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, dignissimos! <br> Lorem ipsum dolor sit amet, consectetur.</p>
        </div>
        <div class="row">
          <div class="col-lg-8 col-md-12 col-xs-12">
            <div class="container">
              <div class="row">
                 <div class="col-lg-6 col-sm-6 col-xs-12 box-item">
                    <span class="icon">
                      <i class="lnr lnr-rocket"></i>
                    </span>
                    <div class="text">
                      <h4>People Counting and Conversion</h4>
                      <p>Get powerful insights such as outside traffic, visit duration, unique traffic, pass-by traffic, visit frequency and entrance path analytics. Up to 99% accuracy. Align labor to increase sales.</p>
                    </div>
                  </div>
                  <div class="col-lg-6 col-sm-6 col-xs-12 box-item">
                    <span class="icon">
                      <i class="lnr lnr-laptop-phone"></i>
                    </span>
                    <div class="text">
                      <h4>Shopper Experience</h4>
                      <p>Movement pattern analysis, assess shopper direction and trends within a space. Understand how your shoppers shop, interact with staff, and optimize your shopper’s experience.</p>
                    </div>
                  </div>
                  <div class="col-lg-6 col-sm-6 col-xs-12 box-item">
                    <span class="icon">
                      <i class="lnr lnr-layers"></i>
                    </span>
                    <div class="text">
                      <h4>Mobile Integration</h4>
                      <p>No app required, available with any device. Machine learning algorithms that gets smarter with each and every interaction between brand and shopper. We send customized offers and content to shoppers to increase loyalty and revenues.</p>
                    </div>
                  </div>
                  <div class="col-lg-6 col-sm-6 col-xs-12 box-item">
                    <span class="icon">
                      <i class="lnr lnr-cog"></i>
                    </span>
                    <div class="text">
                      <h4>Professional Consulting</h4>
                      <p>Dedicated Megatail team oriented to maximize your return on investment. Goal oriented coach and change management process to optimize your stores results. Depth analysis about metrics and collected information, marketing plan development and implementation. Personalized training program for managers and sales team.</p>
                    </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-xs-12">
            <div class="show-box">
              <img class="img-fulid" src="img/features/feature.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Features Section End -->    


<!-- Contact Section Start -->
    <section id="user-form" class="section" data-stellar-background-ratio="-0.2">      
      <div class="contact-form">
        <div class="container">
          <div class="row">     
            <div class="form-block-container col-lg-6 col-sm-6 col-xs-12">
                <h3>Schedule a Demo</h3>
              <div class="form-block">
                <form id="contactForm-2" data-form-id="2">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" class="form-control" id="name" name="name" placeholder="First name" required data-error="Please enter your first name">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" placeholder="Last name" name="surname" id="email" class="form-control" name="name" required data-error="Please enter your last name">
                        <div class="help-block with-errors"></div>
                      </div> 
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" placeholder="Your email" id="email" class="form-control" name="email" required data-error="Please enter your email">
                        <div class="help-block with-errors"></div>
                      </div> 
                    </div>


                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" placeholder="Your phone" id="phone" class="form-control" name="phone" required data-error="Please enter your phone">
                        <div class="help-block with-errors"></div>
                      </div> 
                    </div>


                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" placeholder="Your company" id="company" class="form-control" name="company" required data-error="Please enter your company">
                        <div class="help-block with-errors"></div>
                      </div> 
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <select type="text" placeholder="Your company" id="country" class="form-control" name="country" required data-error="Please enter your company">

                        <option value="" selected="selected" disabled="disabled">Country</option>
                        <option value="519344">United States</option>
                        <option value="519346">Canada</option>
                        <option value="519348">Afghanistan</option>
                        <option value="519350">Albania</option>
                        <option value="519352">Algeria</option>
                        <option value="519354">American Samoa</option>
                        <option value="519356">Andorra</option>
                        <option value="519358">Angola</option>
                        <option value="519360">Anguilla</option>
                        <option value="519362">Antarctica</option>
                        <option value="519364">Antigua and Barbuda</option>
                        <option value="519366">Argentina</option>
                        <option value="519368">Armenia</option>
                        <option value="519370">Aruba</option>
                        <option value="519372">Australia</option>
                        <option value="519374">Austria</option>
                        <option value="519376">Azerbaijan</option>
                        <option value="519378">Bahamas</option>
                        <option value="519380">Bahrain</option>
                        <option value="519382">Bangladesh</option>
                        <option value="519384">Barbados</option>
                        <option value="519386">Belarus</option>
                        <option value="519388">Belgium</option>
                        <option value="519390">Belize</option>
                        <option value="519392">Benin</option>
                        <option value="519394">Bermuda</option>
                        <option value="519396">Bhutan</option>
                        <option value="519398">Bolivia</option>
                        <option value="519400">Bosnia and Herzegovina</option>
                        <option value="519402">Botswana</option>
                        <option value="519404">Brazil</option>
                        <option value="519406">British Indian Ocean Territory</option>
                        <option value="519408">British Virgin Islands</option>
                        <option value="519410">Brunei</option>
                        <option value="519412">Bulgaria</option>
                        <option value="519414">Burkina Faso</option>
                        <option value="519416">Burundi</option>
                        <option value="519418">Cambodia</option>
                        <option value="519420">Cameroon</option>
                        <option value="519422">Cape Verde</option>
                        <option value="519424">Cayman Islands</option>
                        <option value="519426">Central African Republic</option>
                        <option value="519428">Chad</option>
                        <option value="519430">Chile</option>
                        <option value="519432">China</option>
                        <option value="519434">Christmas Island</option>
                        <option value="519436">Cocos (Keeling) Islands</option>
                        <option value="519438">Colombia</option>
                        <option value="519440">Comoros</option>
                        <option value="519442">Congo</option>
                        <option value="519444">Cook Islands</option>
                        <option value="519446">Costa Rica</option>
                        <option value="519448">Croatia</option>
                        <option value="519450">Cuba</option>
                        <option value="519452">Curaçao</option>
                        <option value="519454">Cyprus</option>
                        <option value="519456">Czech Republic</option>
                        <option value="519458">Côte d’Ivoire</option>
                        <option value="519460">Democratic Republic of the Congo</option>
                        <option value="519462">Denmark</option>
                        <option value="519464">Djibouti</option>
                        <option value="519466">Dominica</option>
                        <option value="519468">Dominican Republic</option>
                        <option value="519470">Ecuador</option>
                        <option value="519472">Egypt</option>
                        <option value="519474">El Salvador</option>
                        <option value="519476">Equatorial Guinea</option>
                        <option value="519478">Eritrea</option>
                        <option value="519480">Estonia</option>
                        <option value="519482">Ethiopia</option>
                        <option value="519484">Falkland Islands</option>
                        <option value="519486">Faroe Islands</option>
                        <option value="519488">Fiji</option>
                        <option value="519490">Finland</option>
                        <option value="519492">France</option>
                        <option value="519494">French Guiana</option>
                        <option value="519496">French Polynesia</option>
                        <option value="519498">French Southern Territories</option>
                        <option value="519500">Gabon</option>
                        <option value="519502">Gambia</option>
                        <option value="519504">Georgia</option>
                        <option value="519506">Germany</option>
                        <option value="519508">Ghana</option>
                        <option value="519510">Gibraltar</option>
                        <option value="519512">Greece</option>
                        <option value="519514">Greenland</option>
                        <option value="519516">Grenada</option>
                        <option value="519518">Guadeloupe</option>
                        <option value="519520">Guam</option>
                        <option value="519522">Guatemala</option>
                        <option value="519524">Guernsey</option>
                        <option value="519526">Guinea</option>
                        <option value="519528">Guinea-Bissau</option>
                        <option value="519530">Guyana</option>
                        <option value="519532">Haiti</option>
                        <option value="519534">Honduras</option>
                        <option value="519536">Hong Kong S.A.R., China</option>
                        <option value="519538">Hungary</option>
                        <option value="519540">Iceland</option>
                        <option value="519542">India</option>
                        <option value="519544">Indonesia</option>
                        <option value="519546">Iran</option>
                        <option value="519548">Iraq</option>
                        <option value="519550">Ireland</option>
                        <option value="519552">Isle of Man</option>
                        <option value="519554">Israel</option>
                        <option value="519556">Italy</option>
                        <option value="519558">Jamaica</option>
                        <option value="519560">Japan</option>
                        <option value="519562">Jersey</option>
                        <option value="519564">Jordan</option>
                        <option value="519566">Kazakhstan</option>
                        <option value="519568">Kenya</option>
                        <option value="519570">Kiribati</option>
                        <option value="519572">Kuwait</option>
                        <option value="519574">Kyrgyzstan</option>
                        <option value="519576">Laos</option>
                        <option value="519578">Latvia</option>
                        <option value="519580">Lebanon</option>
                        <option value="519582">Lesotho</option>
                        <option value="519584">Liberia</option>
                        <option value="519586">Libya</option>
                        <option value="519588">Liechtenstein</option>
                        <option value="519590">Lithuania</option>
                        <option value="519592">Luxembourg</option>
                        <option value="519594">Macao S.A.R., China</option>
                        <option value="519596">Macedonia</option>
                        <option value="519598">Madagascar</option>
                        <option value="519600">Malawi</option>
                        <option value="519602">Malaysia</option>
                        <option value="519604">Maldives</option>
                        <option value="519606">Mali</option>
                        <option value="519608">Malta</option>
                        <option value="519610">Marshall Islands</option>
                        <option value="519612">Martinique</option>
                        <option value="519614">Mauritania</option>
                        <option value="519616">Mauritius</option>
                        <option value="519618">Mayotte</option>
                        <option value="519620">Mexico</option>
                        <option value="519622">Micronesia</option>
                        <option value="519624">Moldova</option>
                        <option value="519626">Monaco</option>
                        <option value="519628">Mongolia</option>
                        <option value="519630">Montenegro</option>
                        <option value="519632">Montserrat</option>
                        <option value="519634">Morocco</option>
                        <option value="519636">Mozambique</option>
                        <option value="519638">Myanmar</option>
                        <option value="519640">Namibia</option>
                        <option value="519642">Nauru</option>
                        <option value="519644">Nepal</option>
                        <option value="519646">Netherlands</option>
                        <option value="519648">New Caledonia</option>
                        <option value="519650">New Zealand</option>
                        <option value="519652">Nicaragua</option>
                        <option value="519654">Niger</option>
                        <option value="519656">Nigeria</option>
                        <option value="519658">Niue</option>
                        <option value="519660">Norfolk Island</option>
                        <option value="519662">North Korea</option>
                        <option value="519664">Northern Mariana Islands</option>
                        <option value="519666">Norway</option>
                        <option value="519668">Oman</option>
                        <option value="519670">Pakistan</option>
                        <option value="519672">Palau</option>
                        <option value="519674">Palestinian Territory</option>
                        <option value="519676">Panama</option>
                        <option value="519678">Papua New Guinea</option>
                        <option value="519680">Paraguay</option>
                        <option value="519682">Peru</option>
                        <option value="519684">Philippines</option>
                        <option value="519686">Pitcairn</option>
                        <option value="519688">Poland</option>
                        <option value="519690">Portugal</option>
                        <option value="519692">Puerto Rico</option>
                        <option value="519694">Qatar</option>
                        <option value="519696">Romania</option>
                        <option value="519698">Russia</option>
                        <option value="519700">Rwanda</option>
                        <option value="519702">Réunion</option>
                        <option value="519704">Saint Barthélemy</option>
                        <option value="519706">Saint Helena</option>
                        <option value="519708">Saint Kitts and Nevis</option>
                        <option value="519710">Saint Lucia</option>
                        <option value="519712">Saint Pierre and Miquelon</option>
                        <option value="519714">Saint Vincent and the Grenadines</option>
                        <option value="519716">Samoa</option>
                        <option value="519718">San Marino</option>
                        <option value="519720">Sao Tome and Principe</option>
                        <option value="519722">Saudi Arabia</option>
                        <option value="519724">Senegal</option>
                        <option value="519726">Serbia</option>
                        <option value="519728">Seychelles</option>
                        <option value="519730">Sierra Leone</option>
                        <option value="519732">Singapore</option>
                        <option value="519734">Slovakia</option>
                        <option value="519736">Slovenia</option>
                        <option value="519738">Solomon Islands</option>
                        <option value="519740">Somalia</option>
                        <option value="519742">South Africa</option>
                        <option value="519744">South Korea</option>
                        <option value="519746">South Sudan</option>
                        <option value="519748">Spain</option>
                        <option value="519750">Sri Lanka</option>
                        <option value="519752">Sudan</option>
                        <option value="519754">Suriname</option>
                        <option value="519756">Svalbard and Jan Mayen</option>
                        <option value="519758">Swaziland</option>
                        <option value="519760">Sweden</option>
                        <option value="519762">Switzerland</option>
                        <option value="519764">Syria</option>
                        <option value="519766">Taiwan</option>
                        <option value="519768">Tajikistan</option>
                        <option value="519770">Tanzania</option>
                        <option value="519772">Thailand</option>
                        <option value="519774">Timor-Leste</option>
                        <option value="519776">Togo</option>
                        <option value="519778">Tokelau</option>
                        <option value="519780">Tonga</option>
                        <option value="519782">Trinidad and Tobago</option>
                        <option value="519784">Tunisia</option>
                        <option value="519786">Turkey</option>
                        <option value="519788">Turkmenistan</option>
                        <option value="519790">Turks and Caicos Islands</option>
                        <option value="519792">Tuvalu</option>
                        <option value="519794">U.S. Virgin Islands</option>
                        <option value="519796">Uganda</option>
                        <option value="519798">Ukraine</option>
                        <option value="519800">United Arab Emirates</option>
                        <option value="519802">United Kingdom</option>
                        <option value="519804">United States Minor Outlying Islands</option>
                        <option value="519806">Uruguay</option>
                        <option value="519808">Uzbekistan</option>
                        <option value="519810">Vanuatu</option>
                        <option value="519812">Vatican</option>
                        <option value="519814">Venezuela</option>
                        <option value="519816">Viet Nam</option>
                        <option value="519818">Wallis and Futuna</option>
                        <option value="519820">Western Sahara</option>
                        <option value="519822">Yemen</option>
                        <option value="519824">Zambia</option>
                        <option value="519826">Zimbabwe</option>
                        </select>
                        <div class="help-block with-errors"></div>
                      </div> 
                    </div>

                    <div class="col-md-12">
                      <div class="submit-button text-center">
                        <button class="btn btn-common" id="submit" type="submit">Send</button>
                        <div id="msgSubmit" class="h3 text-center hidden"></div> 
                        <div class="clearfix"></div> 
                      </div>
                    </div>
                  </div>            
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>           
    </section>
    <!-- Contact Section End -->




    <!-- Counter Section Start -->
    <div class="counters section" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row"> 
          <!--
          <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="facts-item">   
              <div class="icon">
                <i class="lnr lnr-clock"></i>
              </div>             
              <div class="fact-count">
                <h3><span class="counter">1589</span></h3>
                <h4>Working Hours</h4>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="facts-item">   
              <div class="icon">
                <i class="lnr lnr-briefcase"></i>
              </div>            
              <div class="fact-count">
                <h3><span class="counter">699</span></h3>
                <h4>Completed Projects</h4>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="facts-item"> 
              <div class="icon">
                <i class="lnr lnr-user"></i>
              </div>              
              <div class="fact-count">
                <h3><span class="counter">203</span></h3>
                <h4>No. of Clients</h4>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="facts-item"> 
              <div class="icon">
                <i class="lnr lnr-heart"></i>
              </div>              
              <div class="fact-count">
                <h3><span class="counter">1689</span></h3>
                <h4>Peoples Love</h4>
              </div>
            </div>
          </div>
        -->
        </div>
      </div>
    </div>
    <!-- Counter Section End -->

        <!-- Contact Section Start -->
    <section id="contact" class="section" data-stellar-background-ratio="-0.2">      
      <div class="contact-form">
        <div class="container">
          <div class="row">     
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="contact-us">
                <h3>Contact With us</h3>
                <div class="contact-address">
                  <p>Cerviño 3542 1D, Capital Federal, Argentina</p>
                  <p class="phone">Phone: <span>+54 9 1158447374</span></p>
                  <!--<p class="email">E-mail: <span>(contact@mate.com)</span></p>-->
                </div>
              </div>
            </div>     
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="contact-block">
                <form id="contactForm" method="POST">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" required data-error="Please enter your name">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" placeholder="Your Email" id="email" class="form-control" name="email" required data-error="Please enter your email">
                        <div class="help-block with-errors"></div>
                      </div> 
                    </div>
                    <div class="col-md-12">
                      <div class="form-group"> 
                        <textarea class="form-control" id="message" name="text" placeholder="Your Message" rows="8" data-error="Write your message" required></textarea>
                        <div class="help-block with-errors"></div>
                      </div>
                      <div class="submit-button text-center">
                        <button class="btn btn-common" id="submit" type="submit">Send Message</button>
                        <div id="msgSubmit" class="h3 text-center hidden"></div> 
                        <div class="clearfix"></div> 
                      </div>
                    </div>
                  </div>            
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>           
    </section>
    <!-- Contact Section End -->

    <!-- Footer Section Start -->
    <footer>          
      <div class="container">
        <div class="row">
          <!-- Footer Links -->
          <div class="col-lg-6 col-sm-6 col-xs-12">
            <ul class="footer-links">
              <li>
                <a href="#">Homepage</a>
              </li>
              <li>
                <a href="#">Services</a>
              </li>
              <li>
                <a href="#">About Us</a>
              </li>
              <li>
                <a href="#">Contact</a>
              </li>
            </ul>
          </div>
          <div class="col-lg-6 col-sm-6 col-xs-12">
            <div class="copyright">
              <p>All copyrights reserved &copy; 2018 - Megatail</p>
            </div>
          </div>  
        </div>
      </div>
    </footer>
    <!-- Footer Section End --> 

    <!-- Go To Top Link -->
    <a href="#" class="back-to-top">
      <i class="lnr lnr-arrow-up"></i>
    </a>
    
    <div id="loader">
      <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
      </div>
    </div>     

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="js/jquery-min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mixitup.js"></script>
    <script src="js/nivo-lightbox.js"></script>
    <script src="js/owl.carousel.js"></script>    
    <script src="js/jquery.stellar.min.js"></script>    
    <script src="js/jquery.nav.js"></script>    
    <script src="js/scrolling-nav.js"></script>    
    <script src="js/jquery.easing.min.js"></script>    
    <script src="js/smoothscroll.js"></script>    
    <script src="js/jquery.slicknav.js"></script>     
    <script src="js/wow.js"></script>   
    <script src="js/jquery.vide.js"></script>
    <script src="js/jquery.counterup.min.js"></script>    
    <script src="js/jquery.magnific-popup.min.js"></script>    
    <script src="js/waypoints.min.js"></script>    
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>   
    <script src="js/main.js"></script>
    <?php endif; ?>
  </body>
</html>
